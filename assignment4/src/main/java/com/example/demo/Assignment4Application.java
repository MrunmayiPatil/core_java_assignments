package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Scanner;
@SpringBootApplication
public class Assignment4Application {

	public static void main(String[] args) {
		SpringApplication.run(Assignment4Application.class, args);
		Scanner sc=new Scanner(System.in);
		System.out.println("how many row you want");
		int n=sc.nextInt();
		int row_no = 1; 
	    int curr_star = 0;
        for (row_no = 1; row_no <= n;) // Loop to print desired pattern
        {
            // If current star count is less than
            // current line number
            if (curr_star < row_no)
            {
                System.out.print ( "* ");
                curr_star++;
                continue;
            }
      
            //  to print a new line
            if (curr_star == row_no)
            {
                System.out.println ("");
                row_no++;
                curr_star = 0;
            }
        }
	}

}
