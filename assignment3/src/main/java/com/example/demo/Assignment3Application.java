package com.example.demo;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3Application {

	public static void main(String[] args) {
		SpringApplication.run(Assignment3Application.class, args);
		System.out.println("Enter 5 digit number here");
		Scanner sc=new Scanner(System.in);
		int number=sc.nextInt();
		int number1=(number/10000)%10;
		int number2=(number/1000)%10;
		int number3=(number/100)%10;
		int number4=(number/10)%10;
		int number5=number%10;
		System.out.println(number1+" "+number2+" "+number3+" "+number4+" "+number5);
	}

}
