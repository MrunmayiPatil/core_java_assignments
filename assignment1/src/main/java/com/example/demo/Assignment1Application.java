package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Scanner;
@SpringBootApplication
public class Assignment1Application {

	public static void main(String[] args) {
		System.out.println("Enter number here: "); 
		Scanner s=new Scanner(System.in); //accept the number
		int t=s.nextInt(); 
		int sum=0;  
		while(t!=0) //iterate the loop till remainder come to zero
		{ 
		 	sum=sum+(t%10); 
			t=t/10; 
		}  
		System.out.println("Total sum is "+sum); 
	}

}
